package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	private Controller controller;
	private Patient patient;
	private Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		controller = Controller.getTestController();
		this.patient = controller.opretPatient("121256-0512", "Jane Jensen", 63.4);
		this.laegemiddel = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	}

	@Test
	public void TestOpretPNOrdination() {
		PN pn = controller.opretPNOrdination(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient,
				laegemiddel, 2);
		assertEquals(laegemiddel, pn.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(pn));
		assertEquals(2.0, pn.getAntalEnheder(), 0.001);
		assertEquals(LocalDate.of(2019, 02, 12), pn.getStartDen());
		assertEquals(LocalDate.of(2019, 02, 15), pn.getSlutDen());
	}

	@Test
	public void TestOpretDagligFastOrdination() {
		DagligFast df = controller.opretDagligFastOrdination(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 03, 12),
				patient, laegemiddel, 1, 1, 2, 0);
		assertEquals(laegemiddel, df.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(df));
		assertEquals(1, df.getDoser()[0].getAntal(), 0.001);
		assertEquals(1, df.getDoser()[1].getAntal(), 0.001);
		assertEquals(2, df.getDoser()[2].getAntal(), 0.001);
		assertEquals(0, df.getDoser()[3].getAntal(), 0.001);
		assertEquals(LocalDate.of(2019, 02, 12), df.getStartDen());
		assertEquals(LocalDate.of(2019, 03, 12), df.getSlutDen());
	}

	@Test
	public void TestOpretDagligSkaevOrdination() {
		LocalTime[] klokkeSlet = new LocalTime[3];
		double[] antalEnheder = new double[3];
		klokkeSlet[0] = LocalTime.of(15, 00);
		klokkeSlet[1] = LocalTime.of(18, 00);
		klokkeSlet[2] = LocalTime.of(21, 00);
		antalEnheder[0] = 1;
		antalEnheder[1] = 2;
		antalEnheder[2] = 1;
		DagligSkaev ds = controller.opretDagligSkaevOrdination(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15),
				patient, laegemiddel, klokkeSlet, antalEnheder);
		assertEquals(laegemiddel, ds.getLaegemiddel());
		assertTrue(patient.getOrdinationer().contains(ds));
		assertEquals(LocalTime.of(15, 00), ds.getKlokkeslet()[0]);
		assertEquals(LocalTime.of(18, 00), ds.getKlokkeslet()[1]);
		assertEquals(LocalTime.of(21, 00), ds.getKlokkeslet()[2]);
		assertEquals(1, ds.getAntalEnheder()[0], 0.01);
		assertEquals(2, ds.getAntalEnheder()[1], 0.01);
		assertEquals(1, ds.getAntalEnheder()[2], 0.01);
		assertEquals(LocalDate.of(2019, 02, 12), ds.getStartDen());
		assertEquals(LocalDate.of(2019, 02, 15), ds.getSlutDen());
	}

	@Test
	public void TestAnbefaletDosisPrDoegn() {
		Patient p1 = patient;
		Patient p2 = new Patient("121256-0513", "Jens Jensen", 24);
		Patient p3 = new Patient("121256-0514", "Jane Hansen", 130);
		Patient p4 = new Patient("121256-0515", "Hans Hansen", 25);
		Patient p5 = new Patient("121256-0516", "Karl Hansen", 120);
		assertEquals(2.4, controller.anbefaletDosisPrDoegn(p2, laegemiddel), 0.001);
		assertEquals(9.51, controller.anbefaletDosisPrDoegn(p1, laegemiddel), 0.001);
		assertEquals(20.8, controller.anbefaletDosisPrDoegn(p3, laegemiddel), 0.001);
		assertEquals(3.75, controller.anbefaletDosisPrDoegn(p4, laegemiddel), 0.001);
		assertEquals(18, controller.anbefaletDosisPrDoegn(p5, laegemiddel), 0.001);
	}

	@Test
	public void TestOrdinationPNAnvendt() {
		PN pn = controller.opretPNOrdination(LocalDate.of(2019, 01, 15), LocalDate.of(2019, 02, 15), patient,
				laegemiddel, 2);
		controller.ordinationPNAnvendt(pn, LocalDate.of(2019, 01, 16));
		assertTrue(pn.getDoseDage().contains(LocalDate.of(2019, 01, 16)));
	}

	@Test
	public void TestAntalOrdinationerPrVaegtPrLaegemiddel() {
		Patient p1 = patient;
		Patient p2 = controller.opretPatient("121256-0513", "Jens Jensen", 24);
		Patient p3 = controller.opretPatient("121256-0514", "Jane Hansen", 130);
		Patient p4 = controller.opretPatient("121256-0515", "Hans Hansen", 25);
		Patient p5 = controller.opretPatient("121256-0516", "Karl Hansen", 120);
		controller.opretPNOrdination(LocalDate.of(2019, 01, 15), LocalDate.of(2019, 02, 15), p1, laegemiddel, 2);
		controller.opretPNOrdination(LocalDate.of(2019, 01, 15), LocalDate.of(2019, 03, 15), p2, laegemiddel, 2);
		controller.opretPNOrdination(LocalDate.of(2019, 01, 15), LocalDate.of(2019, 02, 21), p3, laegemiddel, 2);
		controller.opretPNOrdination(LocalDate.of(2019, 01, 15), LocalDate.of(2019, 04, 15), p4, laegemiddel, 2);
		controller.opretPNOrdination(LocalDate.of(2019, 01, 15), LocalDate.of(2019, 02, 22), p5, laegemiddel, 2);
		assertEquals(1, controller.antalOrdinationerPrVægtPrLægemiddel(15, 24, laegemiddel));
		assertEquals(1, controller.antalOrdinationerPrVægtPrLægemiddel(50, 90, laegemiddel));
		assertEquals(2, controller.antalOrdinationerPrVægtPrLægemiddel(120, 200, laegemiddel));
	}
}
