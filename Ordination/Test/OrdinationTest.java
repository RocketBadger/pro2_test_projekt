package Test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;

public class OrdinationTest {
	
	Patient patient = new Patient("121256-0512", "Jane Jensen", 63.4);
	Laegemiddel l1 = new Laegemiddel ("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	DagligFast fast = new DagligFast (LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient, l1);

	@Test
	public void testGetStartDen() {
		assertEquals(LocalDate.of(2019, 02, 12), fast.getStartDen());
	}

	@Test
	public void testGetSlutDen() {
		assertEquals(LocalDate.of(2019, 02, 15), fast.getSlutDen());
	}

	@Test
	public void testAntalDage() {
		assertEquals(4, fast.antalDage());
	}

	@Test
	public void testGetLaegemiddel() {
		assertEquals(l1, fast.getLaegemiddel());
	}

}
