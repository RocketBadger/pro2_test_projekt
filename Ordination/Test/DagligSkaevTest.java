package Test;

import java.time.LocalDate;
import java.time.LocalTime;

import junit.framework.TestCase;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkaevTest extends TestCase {
	private Patient patient;
	private Laegemiddel lm;
	private LocalTime[] klokkeslet = new LocalTime[3];
	private double[] antalEnheder = new double[3];

	public void setUp() throws Exception {
		patient = new Patient("2000000000", "etNavn", 67.4);
		klokkeslet[0] = LocalTime.of(15, 00);
		klokkeslet[1] = LocalTime.of(18, 00);
		klokkeslet[2] = LocalTime.of(22, 00);
		
		antalEnheder[0] = 2;
		antalEnheder[1] = 1;
		antalEnheder[2] = 2;
	}

	public void testSamletDosis1() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, lm, klokkeslet, antalEnheder);
		ds.opretDosis(klokkeslet[0], 2);
		ds.opretDosis(klokkeslet[1], 1);
		ds.opretDosis(klokkeslet[2], 2);
		double actual = ds.samletDosis();
		assertEquals(15, actual, 0.001);
	}
	
	public void testSamletDosis2() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 12), patient, lm, klokkeslet, antalEnheder);
		ds.opretDosis(klokkeslet[0], 2);
		ds.opretDosis(klokkeslet[1], 1);
		ds.opretDosis(klokkeslet[2], 2);
		double actual = ds.samletDosis();
		assertEquals(5, actual, 0.001);
	}

	public void testDoegnDosis1() {
			DagligSkaev ds = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, lm, klokkeslet, antalEnheder);
			ds.opretDosis(klokkeslet[0], 2);
			ds.opretDosis(klokkeslet[1], 1);
			ds.opretDosis(klokkeslet[2], 2);
			double actual = ds.doegnDosis();
			assertEquals(5, actual, 0.001);
	}
	
	public void testDoegnDosis2() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 12), patient, lm, klokkeslet, antalEnheder);
		ds.opretDosis(klokkeslet[0], 1);
		double actual = ds.doegnDosis();
		assertEquals(1, actual, 0.001);
	}	
}