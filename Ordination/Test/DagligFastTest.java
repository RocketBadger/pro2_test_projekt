package Test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Arrays;

import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Patient;
import ordination.TidspunktFast;

public class DagligFastTest {

	Patient patient = new Patient("121256-0512", "Jane Jensen", 63.4);
	Laegemiddel l1 = new Laegemiddel ("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

	@Test
	public void testOpretDosis() {
		//TC1
		DagligFast fast = new DagligFast (LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient, l1);
		fast.opretDosis(TidspunktFast.MORGEN, 2);
		assertEquals(2, fast.doegnDosis(), 00.1);
		assertEquals(8, fast.samletDosis(), 00.1);
	}
	
	@Test
	public void testDoegnDosisTC1() {
		//TC1
		DagligFast fast = new DagligFast (LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 12), patient, l1);
		fast.opretDosis(TidspunktFast.MORGEN, 1);
		fast.opretDosis(TidspunktFast.MIDDAG, 1);
		fast.opretDosis(TidspunktFast.AFTEN, 1);
		fast.opretDosis(TidspunktFast.NAT, 1);
		assertEquals(4, fast.doegnDosis(),0.001);
	}
	
	@Test
	public void testDoegnDosisTC2() {
		//TC1
		DagligFast fast = new DagligFast (LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 14), patient, l1);
		fast.opretDosis(TidspunktFast.MORGEN, 1);
		fast.opretDosis(TidspunktFast.MIDDAG, 1);
		fast.opretDosis(TidspunktFast.AFTEN, 1);
		fast.opretDosis(TidspunktFast.NAT, 1);
		assertEquals(4, fast.doegnDosis(),0.001);
	}
	
	@Test
	public void testSamletDosisTC1() {
		DagligFast fast = new DagligFast (LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 12), patient, l1);
		fast.opretDosis(TidspunktFast.MORGEN, 1);
		fast.opretDosis(TidspunktFast.MIDDAG, 1);
		fast.opretDosis(TidspunktFast.AFTEN, 1);
		fast.opretDosis(TidspunktFast.NAT, 1);
		assertEquals(4, fast.samletDosis(),0.001);
		
	}
	@Test
	public void testSamletDosisTC2() {
		DagligFast fast = new DagligFast (LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, l1);
		fast.opretDosis(TidspunktFast.MORGEN, 1);
		fast.opretDosis(TidspunktFast.MIDDAG, 1);
		fast.opretDosis(TidspunktFast.AFTEN, 1);
		fast.opretDosis(TidspunktFast.NAT, 1);
		System.out.println(Arrays.toString(fast.getDoser()));
		System.out.println(fast.antalDage());
		assertEquals(12, fast.samletDosis(),0.001);
	}




}
