package Test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PNTest {

	Patient patient = new Patient("121256-0512", "Jane Jensen", 63.4);
	Laegemiddel l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

	@Test
	public void testSamletDosisTC1() {
		PN pn = new PN(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient, l1, 2);
		pn.givDosis(LocalDate.of(2019, 02, 12));
		pn.givDosis(LocalDate.of(2019, 02, 13));
		pn.givDosis(LocalDate.of(2019, 02, 14));
		pn.givDosis(LocalDate.of(2019, 02, 15));
		assertEquals(8, pn.samletDosis(), 00.1);
	}

	@Test
	public void testSamletDosisTC2() {
		PN pn = new PN(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient, l1, 2);
		pn.givDosis(LocalDate.of(2019, 02, 12));
		pn.givDosis(LocalDate.of(2019, 02, 14));
		pn.givDosis(LocalDate.of(2019, 02, 15));
		assertEquals(6, pn.samletDosis(), 00.1);
	}

	@Test
	public void testDoegnDosisTC1() {
		PN pn = new PN(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient, l1, 2);
		pn.givDosis(LocalDate.of(2019, 02, 12));
		pn.givDosis(LocalDate.of(2019, 02, 14));
		assertEquals(1, pn.doegnDosis(), 00.1);
	}

	@Test
	public void testDoegnDosisTC2() {
		PN pn = new PN(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient, l1, 2);
		pn.givDosis(LocalDate.of(2019, 02, 12));
		assertEquals(0.5, pn.doegnDosis(), 00.1);
	}

	@Test
	public void testDoegnDosisTC3() {
		PN pn = new PN(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient, l1, 2);
		pn.givDosis(LocalDate.of(2019, 02, 12));
		pn.givDosis(LocalDate.of(2019, 02, 13));
		pn.givDosis(LocalDate.of(2019, 02, 14));
		pn.givDosis(LocalDate.of(2019, 02, 15));
		assertEquals(2, pn.doegnDosis(), 00.1);
	}

	@Test
	public void testGivDosisT1() {
		PN pn = new PN(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient, l1, 2);
		assertEquals(true, pn.givDosis(LocalDate.of(2019, 02, 13)));
	}

	@Test
	public void testGivDosisT2() {
		PN pn = new PN(LocalDate.of(2019, 02, 12), LocalDate.of(2019, 02, 15), patient, l1, 2);
		assertEquals(false, pn.givDosis(LocalDate.of(2019, 02, 16)));
	}

}
