package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<>();
	private LocalTime[] klokkeslet = new LocalTime[4];
	private double[] antalEnheder = new double[4];

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			LocalTime[] klokkeslet, double[] antalEnheder) {
		super(startDen, slutDen, patient, laegemiddel);
		this.doser = new ArrayList<Dosis>();
		this.klokkeslet = klokkeslet;
		this.antalEnheder = antalEnheder;
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
	}

	@Override
	public double samletDosis() {
		double dose = 0.0;
		for (Dosis dosis : doser) {
			if (dosis != null) {
				dose += dosis.getAntal() * antalDage();
			}
		}
		return dose;
	}

	@Override
	public double doegnDosis() {
		double dose = 0.0;
		for (Dosis dosis : doser) {
			if (dosis != null) {
				dose += dosis.getAntal() * antalDage();
			}
		}
		return dose / antalDage();
	}

	public LocalTime[] getKlokkeslet() {
		return Arrays.copyOf(klokkeslet, klokkeslet.length);
	}

	public double[] getAntalEnheder() {
		return Arrays.copyOf(antalEnheder, antalEnheder.length);
	}

	@Override
	public String getType() {
		return "TIDS BESTEMT";
	}
}
