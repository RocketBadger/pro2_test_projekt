package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> doseDage = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel, double antal) {
		super(startDen, slutDen, patient, laegemiddel);
		this.antalEnheder = antal;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if ((givesDen.isAfter(getStartDen()) || givesDen.isEqual(getStartDen())) &&
				(givesDen.isBefore(getSlutDen()) || givesDen.isEqual(getSlutDen()))) {
			doseDage.add(givesDen);
			return true;
		} else {
			return false;
		}
	}

	// (antal gange ordinationen er anvendt * antal enheder) / (antal dage mellem
	// første og sidste givning)
	// Pre: doser registreres i kronologisk rækkefølge
	@Override
	public double doegnDosis() {
		if (getAntalGangeGivet() > 0) {
			return (getAntalGangeGivet() * getAntalEnheder())
					/ antalDage();
		} else {
			return 0;
		}
	}

	@Override
	public double samletDosis() {
		return getAntalGangeGivet() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		int antalGange = 0;
		for (int i = 0; i < doseDage.size(); i++) {
			antalGange++;
		}
		return antalGange;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public String getEnhed() {
		return getLaegemiddel().getEnhed();
	}

	public ArrayList<LocalDate> getDoseDage() {
		return new ArrayList<>(doseDage);
	}

	@Override
	public String getType() {
		return "PN ordinering";
	}

}
