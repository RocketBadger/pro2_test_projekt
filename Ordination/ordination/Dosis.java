package ordination;

import java.time.LocalTime;

public class Dosis {
	private LocalTime tid;
	private TidspunktFast tidspunktFast;
	private double antal;

	public Dosis(LocalTime tid, double antal) {
		super();
		this.tid = tid;
		this.antal = antal;
	}

	public Dosis(TidspunktFast tidspunkt, double antal) {
		this.tidspunktFast = tidspunkt;
		this.antal = antal;
	}

	public Dosis(Double antal) {
		super();
		this.antal = antal;
	}

	public double getAntal() {
		return antal;
	}

	public void setAntal(double antal) {
		this.antal = antal;
	}

	public LocalTime getTid() {
		return tid;
	}

	public void setTid(LocalTime tid) {
		this.tid = tid;
	}

	public TidspunktFast getTidspunktFast() {
		return tidspunktFast;
	}

	@Override
	public String toString() {
		return "Kl: " + tid + "   antal:  " + antal;
	}

}
