package ordination;

import java.time.LocalDate;
import java.util.Arrays;

public class DagligFast extends Ordination {

	// "Med fast menes, at medicinen højst gives 4 gange i døgnet"
	private Dosis[] doser = new Dosis[4];

	// fungerer denne med Controller.opretDagligFastOrdination metoden?
	public DagligFast(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel) {
		super(startDen, slutDen, patient, laegemiddel);
	}

	public void opretDosis(TidspunktFast TidspunktFast, double antal) {
		Dosis dosis = new Dosis(TidspunktFast, antal);
		if (dosis.getTidspunktFast() == ordination.TidspunktFast.MORGEN) {
			doser[0] = dosis;
		} else if (dosis.getTidspunktFast() == ordination.TidspunktFast.MIDDAG) {
			doser[1] = dosis;
		} else if (dosis.getTidspunktFast() == ordination.TidspunktFast.AFTEN) {
			doser[2] = dosis;
		} else if (dosis.getTidspunktFast() == ordination.TidspunktFast.NAT) {
			doser[3] = dosis;
		}
	}

	@Override
	// antalDage() metoden er fra superklassen (Ordination)
	public double samletDosis() {
		return doegnDosis() * super.antalDage();
	}

	@Override
	// getAntal() metoden er fra "Dosis" - klassen
	public double doegnDosis() {
		double dosis = 0;
		for (int i = 0; i < doser.length; i++)
			if (doser[i] != null) {
				dosis = dosis + doser[i].getAntal();
//			} else {
//				dosis = 0; //Sætter denne linje ikke bare hele døgndosen til 0 hvis bare èt tidspunkt ikke indeholder en dosis? 
//				Dvs at man ikke kan tage medicinen < 4 i døgnet, uden at døgndosen bliver == 0?
			}
		return dosis;
	}

	@Override
	public String getType() {
		return "FAST DOSIS";
	}

	public Dosis[] getDoser() {
		return Arrays.copyOf(doser, doser.length);
	}
}
